#include <stdio.h>
#include <stdlib.h>

typedef struct element{
  struct element *next, *prev;
  int id1;
  int id2;
}element;

typedef struct list{
  element *first, *last;
  int size;
}list;

// Oraz trzy funkcje, kopiująca tworzy element na
// podstawie wskaźnika do obiektu *E, tworząca element
// na podsprzez wskazanie wartości pól id1,id2,dodająca element *E
// na koniec listy *L.
//

void list_init(list *L){
  L->first = NULL; //To samo co (*L).first = NULL
  L->last = NULL;
  L->size=0;
}

element * element_copy(element *E){
    //EC = Element_Copy
    element *EC = malloc(sizeof(element));
    EC -> id1 = E->id1;
    EC -> id2 = E->id2;
    EC -> next = E->next;
    EC -> prev = E->prev;
    return EC;
}

element *create_element(int new_ID1, int new_ID2){

  element *E = malloc(sizeof(element));
  E->id1=new_ID1;
  E->id2=new_ID2;
  E->next=NULL;
  E->prev=NULL;
  return E;

}

void add_element_2_end_list(list *L, element *E){

    L->size++;
    if(L->first == NULL){
      L->first = E;
      L->last = E;
    }
    else{
      L->last->next = E;
      E->prev = L->last;
      L->last=E;
    }

}

void add_elemnt_2_begin_list(list *L, element *E){
    L->size++;
    if(L->first == NULL){
      L->first = E;
      L->last = E;
    }
    else{
      L->first->prev=E;
      E->next=L->first;
      L->first=E;
      L->size++;
    }
}

void print_element(element E){
    printf(" id1: %d, id2: %d\n", E.id1,E.id2);
}

void print_list(list L){

    element * E;
    E=L.first;
    while (E!=NULL)
    {
        print_element(*E);
        E=E->next;
    }

}

void free_list(list L){
  element *E, *Temp;
  E = L.first;
  while(E){
    Temp = E;
    E = E->next;
    free(Temp);
  }
}

int max_Id1(list L){ // 1a wartość maksymalnego id1 jeśli lista jest niepusta;

    int max_element;
    element *E;
    E = L.first;
    if(E!=NULL){
      max_element = E->id1;

      while(E!=NULL){
        if(max_element < E->id1)
          max_element = E->id1;

        E=E->next;
      }
      return max_element;

    }


}

int parity(list L){ //1b
	element * E;
    E=L.first;
    int counter=0;
    while (E!=NULL){
    	if((E->id1)%2!=(E->id2)%2){
    		counter++;
		}
		E=E->next;
	}
	return counter;
}


int number_Div(list L){ //1c
  element *E;
  E = L.first;
  int counter_number = 0;
  int counter_div = 0;
  while(E!=NULL){

    for(int i = 1;i <= (E -> id1);i++){
      if((E -> id1)%i == 0){
        counter_div++;
      }
    }

    if(counter_div == E -> id2){
      counter_number++;
    }
    counter_div = 0;
    E=E->next;
  }
  return counter_number;
}

int f_numbers(list L){ //1d

  element *E;
  E = L.first;
  int counter_number=0;
  int sumA=0;
  int sumB=0;

  while(E!=NULL){
    for(int i = 1;i<(E-> id1);i++){
      if((E->id1)%i == 0){
        sumA=sumA+i;
      }
    }
    for(int i = 1;i<(E-> id2);i++){
      if((E->id2)%i == 0){
        sumB=sumB+i;
      }
    }
    if(sumA == E->id2 && sumB == E->id1){
      counter_number++;
    }
    E=E->next;
    sumA=0;
    sumB=0;
  }
  return counter_number;
}


int tr_numbers(list L){ // 1E
  element *E;
  E = L.first;
  int counter_number=0;
  int Tn=0;
  while(E!=NULL){
    for(int i=0;i<(E->id1);i++){
      Tn=(i*(i+1))/2;
      if(Tn==E->id1){
        counter_number++;
      }
      Tn=0;
    }

    E=E->next;

  }
  return counter_number;
}

int is_Fib(int n) // Funkcja pomocnicza do 1F
{
    int a = 0;
    int b = 1;
    if (n==0 || n==1)
      return 1;
    int c = a+b;
    while(c<=n)
    {
        if(c == n)
          return 1;
        a = b;
        b = c;
        c = a + b;
    }
    return 0;
}

int number_Fib(list L){ //1F
  element *E;
  E = L.first;
  int counter_number=0;
  while(E!=NULL){

      if(is_Fib(E->id1)==1){
        counter_number++;
      }


    E=E->next;
  }
  return counter_number;
}

void save_List_To_File(list L){
  FILE *F;
  F = fopen("dane.txt","w");
  element *E;
  E = L.first;

  while(E!=NULL){
    fprintf(F,"%d,%d,",E->id1,E->id2);
    E = E->next;
  }
  fclose(F);

}

list create_List_From_File(){
    FILE *F;
    F = fopen("dane.txt","r");
    list L;
    int temp1,temp2;
    list_init(&L);
    while((fscanf(F,"%d,%d,",&temp1,&temp2))>0){
      add_element_2_end_list(&L,create_element(temp1,temp2));
    }
    return L;

  }

void move_list(list L,list L2){
    element *E,*E2;
    E=L.first;
    E2=L2.last;
    while(E!=NULL){
      add_element_2_end_list(&L2,create_element(E->id1,E->id2));
      E=E->next;
      E2=E2->next;
    }
    free_list(L);
}

void copy_2x_element(list L,list L2){
  element *E,*E2;
  E=L.last;
  E2=L2.first;
  int tmp_size = L2.size;
  int counter=0;

    while(E2!=NULL){
      if((counter+1)!=tmp_size){
        E2=E2->next;
        add_element_2_end_list(&L,create_element(E2->id1,E2->id2));
        E=E->next;

        E2=E2->next;
        counter=counter+2;
    }else{
      E2=E2->next;
    }
  }

}

void print_2x_element_from_end(list L){
  element *E;
  E=L.last;
  int tmp_size = L.size;


    while(E){
    if((tmp_size-1)!=0){
      E=E->prev;
      print_element(*E);
      E=E->prev;
      //E=E->prev;
      tmp_size=tmp_size-2;
    }else{
      E=E->prev;
    }
  }
}

int main(){

  list L,LT;

  list_init(&L);
  list_init(&LT);
  add_element_2_end_list(&L,create_element(7,5));
  add_element_2_end_list(&L,create_element(5,5));
  add_element_2_end_list(&L,create_element(10,1));
  add_element_2_end_list(&L,create_element(3,4));
  add_element_2_end_list(&L,create_element(6,4));
  add_element_2_end_list(&L,create_element(220,284));
  add_element_2_end_list(&L,create_element(1184,1210));
  add_element_2_end_list(&L,create_element(10,1210));
  add_element_2_end_list(&L,create_element(11,32));
  add_element_2_end_list(&L,create_element(56,322323));
  add_element_2_end_list(&LT,create_element(13,45));
  add_element_2_end_list(&LT,create_element(15,67));
  add_element_2_end_list(&LT,create_element(123,56));
  add_element_2_end_list(&LT,create_element(256,123));
  add_element_2_end_list(&LT,create_element(6,123));
  add_element_2_end_list(&LT,create_element(256,234));
  //move_list(L,LT);

  //copy_2x_element(L,LT);
  print_2x_element_from_end(L);
  // printf("-----------------\n");
  // print_list(L);
  // printf("-----------------\n");
  // print_list(LT);
//  printf("ile liczb podzielnych %d\n",number_Div(L));
//  printf("Liczb fb %d\n",number_Fib(L));

  free_list(LT);
  free_list(L);



  return 0;
}
